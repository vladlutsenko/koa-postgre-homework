

import Koa from 'koa';


const app = new Koa();

import userRouter from './user/router';
import bodyParser from 'koa-bodyparser';

app.use(bodyParser());
app.use(userRouter.routes());

app.listen(3000, () => {
  console.log('server started')
})
