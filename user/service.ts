// TODO: implement db calls


import db from '../db';
import User from '../db/user';

export async function getUsers() {
  try {
    const result = await db.query(`SELECT id, name, email FROM "users"`);
    console.log(result.rows);
    return result.rows;
  } catch (err) {
    throw new Error(err.message);
  } 
}

export async function addUser(user: User) {
  try {
    const result = await db.query(`INSERT INTO "users" (name, email) VALUES ('${user.name}', '${user.email}') RETURNING id`);
    console.log(result.rows);
    return result.rows;
  } catch (err) {
    console.error(err.message);
    throw new Error(err.message);
  } 
}

export async function updateUser(id: number, user: User) {
  try {
    await db.query(`UPDATE "users" SET name = '${user.name}', email = '${user.email}' WHERE id = ${id}`);
    return true;
  } catch (err) {
    console.error(err.message);
    throw new Error(err.message);
  } 
}

export async function deleteUser(id: number) {
  try {
    await db.query(`DELETE FROM "users" WHERE id = ${id}`);
    return true;
  } catch (err) {
    throw new Error(err.message);
  } 
}
