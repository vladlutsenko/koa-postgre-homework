// TODO: create a router and bind endpoints here
import Router from 'koa-router';
import { handleUserGet,  handleUserAdd, handleUserUpdate, handleUserDelete} from './controller';

const router = new Router({ prefix: '/user' });

router.get('/', handleUserGet);
router.post('/', handleUserAdd);
router.post('/:id', handleUserUpdate);
router.delete('/:id', handleUserDelete);

export default router;
