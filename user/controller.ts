// todo: implement service calls and write function to handle routes


import { Context } from 'koa';
import { getUsers, addUser, updateUser, deleteUser } from './service';
import User from '../db/user';

export async function handleUserGet(ctx: Context) {
  const users = await getUsers();
  ctx.body = users;
  ctx.status = 200;
}

export async function handleUserAdd(ctx: Context) {
  const user = ctx.request.body as User;
  try {
    const userID = await addUser(user);
    ctx.body = userID;
    ctx.status = 200;
  } catch (error) {
    ctx.status = 400;
  }
}

export async function handleUserUpdate(ctx: Context) {
  const id = ctx.params.id;
  const user = ctx.request.body as User;
  try {
    const response = await updateUser(id, user);
    ctx.body = response;
    ctx.status = 200;
  } catch (error) {
    ctx.status = 400;
  }
}

export async function handleUserDelete(ctx: Context) {
  const id = ctx.params.id;
  try {
    const response = await deleteUser(id);
    ctx.body = response;
    ctx.status = 200;
  } catch (error) {
    ctx.status = 400;
  }
}
