# Lecture 13 homework

## Start server + db

```
docker-compose up
```

## Endpoints
 - get users:  
    >METHOD: GET
    ```
    http://localhost:3000/user
    ```

 - add user:  
    >METHOD: POST  
    BODY: name: string, email: string
    ```
    http://localhost:3000/user
    ```

 - update user:  
    >METHOD: POST  
    BODY: name: string, email: string
    ```
    http://localhost:3000/user/:id
    ```

 - delete user:  
    >METHOD: DELETE  
    ```
    http://localhost:3000/user/:id
    ```