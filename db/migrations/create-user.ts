import { Pool } from 'pg';
import config from '../../config/database';

// TODO: write here sql scripts for initial execution (create user table for ex.)

const pool = new Pool(config)
pool.query(`
    CREATE TABLE IF NOT EXISTS "users" (
        id serial PRIMARY KEY ,
         name varchar(255),
          email varchar(255) UNIQUE)
  `).then(() => {
      console.log('migration complete');
});
