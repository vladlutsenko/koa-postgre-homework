

// TODO: add user interface and logic for storing and fetching

export default interface User {
    id?: number;
    name: string;
    email: string;
}